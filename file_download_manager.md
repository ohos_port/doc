# OpenHarmony固件下载管理

## OHOS-4.0 Release版本

#### ohos-4.0-release-oneplus-fajita-20231209

- 更新说明：升级到4.0 Release版本，新开发了主题商店应用，并内置在系统固件中，支持预置壁纸设置，也支持线上壁纸下载和设置。
- 存在问题：录音机应用存在异常，部分应用启动渲染过慢，应用启动时直接返回会出现卡顿。
- 下载链接
    - 适用于双系统：百度网盘（链接：https://pan.baidu.com/s/1mTYzAOYXHHKqQs9Htnw-CQ 提取码：h3y8）
    - 适用于单系统：百度网盘（链接：https://pan.baidu.com/s/19sNWCey_Cb6e7QDzUf3A7Q 提取码：3vw3）


## OHOS-3.2 Release版本

#### ohos-3.2-release-oneplus-fajita-20231231

- 更新说明：初步实现通话和短信功能，但由于受到基站信号和不同运营商等因素影响，无法做到完全兼容，存在部分功能受限和不能使用的问题，可以提issues反馈。
- 下载链接
    - 适用于双系统：百度网盘（链接：https://pan.baidu.com/s/1w8p72IDFc2eH6c5nvmwgiA 提取码：4ccx）
    - 适用于单系统：百度网盘（链接：https://pan.baidu.com/s/1pnCLtxU5sL6_r5woOoKF8w 提取码：qz68）

#### ohos-3.2-release-oneplus-fajita-20230806

- 更新说明：完成音频适配，优化电源管理和休眠，内核新增hmdfs特性，修复文件管理异常，新增预装录音机应用，中文输入法异步任务优化，FOH应用市场更新至1.3.1版本。
- 存在问题：开机时音频存在一定概率的使能异常，没有声音时需要重启才能生效。
- 下载链接
    - 适用于双系统：百度网盘（链接：https://pan.baidu.com/s/1liq1MJkKx3Ajt_u3FDq8DQ 提取码：2xp2）
    - 适用于单系统：百度网盘（链接：https://pan.baidu.com/s/1HwOT8tjaVoVJx6cWIWJ2JA 提取码：kmz9）

#### ohos-3.2-release-oneplus-fajita-202300804

- 更新说明：适配音频，解决锁屏USB断连问题。
- 存在问题：蓝牙设备连接失败，音频输出存在杂音，中文输入法存在卡顿需要优化，无法读取公共空间。
- 下载链接
    - 适用于双系统：坚果云 https://www.jianguoyun.com/p/DTJpY9cQ2sPjCBjYnZMFIAA (访问密码：51C428)

#### ohos-3.2-release-oneplus-fajita-20230415

- 更新说明：初步完成蓝牙驱动适配，修复备忘录应用闪退问题，支持开源中文输入法，WebView可以正常启动、集成Demo版浏览器并做定制适配，集成F-OH应用市场，高级重启应用支持进入Bootloader模式。
- 存在问题：蓝牙设备连接失败，多媒体功能无法正常使用，中文输入法存在卡顿需要优化。
- 下载链接
    - 适用于双系统：百度网盘（链接：https://pan.baidu.com/s/18AJ1XzlZ1RgRXs0fBFWhrw 提取码：hfuo）
    - 适用于单系统：百度网盘（链接：https://pan.baidu.com/s/1lZVy56azI8jCDCHiTNFJ4Q 提取码：5vpk）

## OHOS-4.0.x 主线版本
ohos-4.0.5.5-oneplus-fajita-2023-03-29

- 更新说明：加入高级重启功能，支持切换A/B槽，完成WiFi驱动适配，优化电池电量、无网络时间保持和屏幕亮度调节。
- 存在问题：备忘录应用闪退、截图无法保存、图库无新增图片、webview启动失败。
- 下载链接
    - 适用于双系统：百度网盘（链接：https://pan.baidu.com/s/1QMHsmnni7ervkuOmh6Ri1g 提取码：44va）
    - 适用于单系统：123云盘（链接：https://www.123pan.com/s/EKF7Vv-Gkei.html 提取码：Mg22）