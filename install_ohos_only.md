# 一加6t仅刷入OHOS教程

> 国行、海外不同的机型有不同的情况  
> 建议先9008回官方安卓9作为底包  
> 根据现有网友刷机总结出来的经验

- 下载并解压适用于单系统的固件
- 解锁后进入BootLoader，运行固件解压目录下的install脚本
- 如果开机进原系统，刷下boot_a（当前为b分区启动）
```
fastboot erase dtbo_a
fastboot erase boot_a
fastboot flash boot_a boot.img
fastboot reboot
```
- 如果还不行并且开机就进BL，设置从a分区启动
```
fastboot set_active a
fastboot reboot
```
- 如果还不行（暂时还没碰到），9008回官方继续折腾...
