# 一加6t双系统刷入教程

手机需要升级到H2OS 11系统。
<p align="center">
<img src="https://foruda.gitee.com/images/1679762980343629028/1ba6e05a_4854455.jpeg" width="30%" >
</p>

### 解锁Bootloader

在开发者选项里面打开使用oem解锁，以及高级重启
<p align="center">
<img src="https://foruda.gitee.com/images/1679763170968045351/9a56bac4_4854455.jpeg" width="30%" >
</p>

关机，按【音量+】【音量-】【电源】组合键，重启到bootloader界面，可以看到设备状态为locked（未解锁），需要先进行解锁操作。
<p align="center">
<img src="https://foruda.gitee.com/images/1679766258238303667/d73b19dd_4854455.jpeg" width="30%" >
</p>
设备连接到电脑，在powershell输入以下解锁命令：

```
fastboot oem unlock
```
按照提示进行操作，【音量+/-】为上下键，【电源】键确认。
<p align="center">
<img src="https://foruda.gitee.com/images/1679766399580928463/e26aa1de_4854455.jpeg" width="30%" >
</p>

等待设备重启。

### 新增OpenHarmony用户数据分区
该方案是暂时的，目的是隔离安卓的用户分区，影响是不能数据共享，后面有没有其他更好的解决方案。

通过以下命令启动TWRP：

```
fastboot boot .\TWRP-3.7.0_12-fajita.img
```
parted工具获取链接：https://pan.baidu.com/s/1mnZPl7uPo5J0R9WIKGJQIQ 
提取码：d1c8
用adb将分区管理软件上传到设备，命令如下：

```
adb push .\parted\parted /sdcard
adb shell
cp /sdcard/parted /sbin/
chmod 755 /sbin/parted
```
<p align="center">
<img src="https://foruda.gitee.com/images/1679776835214264604/3cb97e6a_4854455.png" width="50%" >
</p>

创建分区

```
umount /data && umount /sdcard
parted /dev/block/sda
resizepart 17
# 62GB
mkpart ohdata ext4 62GB 125GB
# p 查看分区
# quit 退出parted
```

<p align="center">
<img src="https://foruda.gitee.com/images/1679776856727724868/8d0d5e79_4854455.png" width="50%" >
</p>

## 刷入LineageOS(Android13)

### 下载固件

[lineageos固件](https://download.lineageos.org/devices/fajita/builds)

下载刷机所需要的文件，包括lineage-20.0-xxxxxxx-nightly-fajita-signed.zip、boot.img、dtbo.img、vbmeta.img。
重启到bootloader，使用以下命令刷入固件：

### 先设置为A槽，刷入文件

```
fastboot set_active a
fastboot erase dtbo
fastboot flash dtbo dtbo.img
fastboot erase vbmeta
fastboot flash vbmeta vbmeta.img
fastboot erase boot
fastboot flash boot boot.img
```
### 再设置为B槽，刷入文件

```
fastboot set_active b
fastboot erase dtbo
fastboot flash dtbo dtbo.img
fastboot erase vbmeta
fastboot flash vbmeta vbmeta.img
fastboot erase boot
fastboot flash boot boot.img
```
关机情况下，按【电源】和【音量-】组合键，进入Recovery模式。
确认当前活动槽为B槽，因为备份模式，系统将会安装在A槽。
选择“Apply Update”, 接着选“Apply from ADB”，然后等待sideload。
输入以下命令：

```
adb sideload lineage-20.0-xxxxxxx-nightly-fajita-signed.zip
```
<p align="center">
<img src="https://foruda.gitee.com/images/1679765908225156143/63fdb011_4854455.png" width="50%" >
</p>

<p align="center">
<img src="https://foruda.gitee.com/images/1679765895055699122/09ea90c8_4854455.jpeg" width="30%" >
</p>

等待安装完成，进入安卓系统。

### 刷入TWRP

[下载TWRP](https://sourceforge.net/projects/oneplus-6-series/files/A12%20TWRP/Fajita/)选择TWRP-3.7.0_12-fajita版本，分别下载img和zio文件。

重启进入bootloader，通过以下命令启动TWRP：

```
fastboot boot .\TWRP-3.7.0_12-fajita.img
```
刷入TWRP，选择【高级】->【ADB Sideload】，命令行输入：
```
adb sideload .\TWRP-3.7.0_12-fajita.zip
```
<p align="center">
<img src="https://foruda.gitee.com/images/1679768080720515940/1e7c572a_4854455.png" width="50%" >
</p>

<p align="center">
<img src="https://foruda.gitee.com/images/1679768205099370961/b495d1ab_4854455.jpeg" width="30%" >
</p>


## 刷入OpenHarmony

[获取固件](https://gitee.com/ohos_port/doc/wikis/%E4%B8%80%E5%8A%A06t%E5%BC%80%E6%BA%90%E9%B8%BF%E8%92%99%E5%9B%BA%E4%BB%B6%E4%B8%8B%E8%BD%BD%E7%AE%A1%E7%90%86)

解压后得到：
<p align="center">
<img src="https://foruda.gitee.com/images/1679777957036828511/907e1d88_4854455.png" width="50%" >
</p>

重启进入Bootloader模式，在windows模式下，运行install.bat脚本，linux下执行install.sh脚本。
等待设备重启，进入OpenHarmony系统。
