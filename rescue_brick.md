# 一加6t救砖教程

下载一加6t线刷包：
https://cloud.189.cn/t/7zuMjiIvmqqa (访问码:0kgv)

解压线刷包，内容如图所示：

<p align="center">
<img src="https://foruda.gitee.com/images/1679753683581230140/472fcd45_4854455.png" width="60%" >
</p>

右键以管理员模式打开MsmDownloadTool V4.0.exe：
<p align="center">
<img src="https://foruda.gitee.com/images/1679753831965746481/64f65835_4854455.png" width="60%" >
</p>

拔除usb数据线，将设备关机，同时按下音量+和音量-组合键，再连接usb数据线，设备进入EDL模式。
 **（注意：若要退出EDL模式，需要重启设备并按住电源键10-15秒）** 
此时，设备管理器出现高通9008串口：
<p align="center">
<img src="https://foruda.gitee.com/images/1679754373951674312/0c87a13c_4854455.png" width="60%" >
</p>

MsmDownloadTool界面上也出现串口信息，状态为已连接，说明设备已经准备好了，可以进行刷机。
<p align="center">
<img src="https://foruda.gitee.com/images/1679754534286938270/d2d9fdca_4854455.png" width="60%" >
</p>

点击左上角的“start”按钮，开始进行刷机，并等待下载完成。
<p align="center">
<img src="https://foruda.gitee.com/images/1679755219121494862/e211c278_4854455.png" width="60%" >
</p>

完成后关掉软件，手机自动重启。