文档基于Ubuntu20.04版本构建

## 1. 配置开发环境

### 1.1 安装依赖

```
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex          \
bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib            \
libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache     \
libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8              \
python3-pip ruby device-tree-compiler lib32stdc++6 lib32z1 libncurses5-dev           \
libtinfo5 scons genext2fs abootimg -y
```

### 1.2 配置git

```
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store
```

### 1.3 安装repo工具

下载文件
```
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > bin/repo
chmod a+x bin/repo
```
将bin路径添加到PATH，

```
gedit ~/.bashrc
# 添加到文件最后：export PATH=$PATH:～/bin
source ~/.bashrc
```
配置地址

```
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```

### 1.4 同步OpenHarmony源码

创建项目文件夹

```
mkdir ohos
cd ohos
```

获取OpenHarmony 3.2-Release代码
```
repo init -u https://gitee.com/ohos_port/manifest -b fajita-OpenHarmony-3.2-Release --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

获取OpenHarmony 4.0-Release代码
```
repo init -u https://gitee.com/ohos_port/manifest -b OpenHarmony-4.0-Release-Fajita --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

执行prebuilts

```
bash build/prebuilts_download.sh
```

执行oneplus补丁

```
bash device/board/oneplus/init_patch.sh
```

## 2. 编译与打包

### 2.1 编译OpenHarmony

进入源码根目录，执行如下命令进行编译：

```
./build.sh --product-name fajita –ccache
```

编译成功时，结果显示如下：

```
[OHOS INFO] fajita build success
```

### 2.1 解决编译异常

fatal error: 'X11/Xcursor/Xcursor.h' file not found
```
sudo apt-get install libxcursor-dev
```
  
fatal error: 'X11/extensions/Xrandr.h' file not found
```
sudo apt-get install libxrandr-dev
```
  
fatal error: 'X11/extensions/Xinerama.h' file not found
```
sudo apt-get install libxinerama-dev
```

### 3.3 打包到安装目录

- `boot.img` ~/ohos/out/fajita/packages/phone/images/boot.img
- `system.img` ~/ohos/out/fajita/packages/phone/images/system.img
- `vendor.img` ~/ohos/out/fajita/packages/phone/images/vendor.img
- `userdata.img` ~/ohos/out/fajita/packages/phone/images/userdata.img