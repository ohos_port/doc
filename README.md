### 组织介绍
OpenHarmony移植，由于开源鸿蒙的软件生态尚未完善，希望能装入双系统，并可以自由切换，提高设备使用率。

 **【注意：刷机有风险！！！如果出现意外情况，请查看[一加6t救砖教程](./rescue_brick.md)】** 

### 正在支持：
- 一加6t(oneplus fajita) (arm64)
- 小米pad5(xiaomi nabu) (arm64)
- 赛昉VisionFive2(starfive) (riscv)

设备适配情况
(Y：已支持、P：进行中、N：不支持)
| 设备  | 系统版本 | 内核  | hdc  | 显示  | 触摸 |Wifi  | 蓝牙  | 音频  | 相机  | 电话  | 短信  | 移动数据  | NFC  | GPS  |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| 一加6t  |  OHOS 5.0  |Y  |  Y | Y  | Y  | Y  | N  | Y  | N  | Y  | Y  |  N | N | N |
| 小米pad5 | OHOS 5.0  | Y  |  Y | Y  | Y  | Y  | N  | N  | N  | N  | N  |  N | N | N |
| 赛昉VisionFive2 | OHOS 3.2  | Y  |  Y | N  | N  | N  | N  | N  | N  | N  | N  |  N | N | N |

### 预置应用
十分感谢提供预置应用的个人开发者和团队，为开源鸿蒙提供了更好的应用体验支持！也欢迎更多的应用开发者提供更丰富的应用支持。

| 序号  | 应用名称  | 简介  | 作者  | 是否开源 |
|---|---|---|---|---|
| 1 | [中文输入法](https://gitee.com/ohos_port/applications_inputmethod) | 基于ArkTs移植Pinyin IME实现简易中文输入 | [wathinst](https://gitee.com/wathinst) | 开源 |
| 2 | [主题](https://gitee.com/wathinst/applications_theme) | 开源鸿蒙壁纸主题设置应用 | [wathinst](https://gitee.com/wathinst) | 未开源 |
| 3 | [设置](https://gitee.com/ericple/applications_settings_6t) | 针对一加6T优化的系统设置应用 | [ericple](https://gitee.com/ericple) | 开源 |
| 4 | [悦览浏览器](https://charactech.cn) | 一款纯净易用的原生鸿蒙浏览器 | Charactech | 闭源 |
| 5 | [分发中心](https://gitee.com/laval-applications/laval-dispatch-center) | 开源鸿蒙应用分发管理 | Laval社区 | 开源 |

### 如何刷机

[ohos固件下载](./file_download_manager.md)

[一加6t双系统刷入教程](./install_ohos_and_android.md)

[一加6t仅刷入OHOS教程](./install_ohos_only.md)

### 如何编译

[源码下载和编译](./source_code_build.md)

### 移植文档

[开源鸿蒙(Openharmony3.2 Release)适配alsa音频框架调试](https://gitee.com/ohos_port/doc/wikis/%E5%BC%80%E6%BA%90%E9%B8%BF%E8%92%99(Openharmony3.2%20Release)%E9%80%82%E9%85%8Dalsa%E9%9F%B3%E9%A2%91%E6%A1%86%E6%9E%B6%E8%B0%83%E8%AF%95)

### Wiki

[Wiki页面](https://gitee.com/ohos_port/doc/wikis/%E4%B8%8B%E8%BD%BD%E5%92%8C%E7%BC%96%E8%AF%91)

### 如何加入
请发送申请邮件至wxz@xkzhineng.com

### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

<p>
<img src="https://foruda.gitee.com/images/1678582965672062493/291a4551_4854455.jpeg" width="15%" >
</p>
